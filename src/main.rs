#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate juniper;
extern crate juniper_rocket;
extern crate rocket;
extern crate pretty_env_logger;
#[macro_use] extern crate log;

mod eventsourcing;
mod graphql;

use std::thread;
use std::sync::mpsc::{channel};

fn main() {
    pretty_env_logger::init();

    let (tx, rx) = channel();

    thread::spawn(move || for cmd in rx.iter() {
        debug!("Received command: {:?}", cmd);

    });

    graphql::server::start(tx);
}
