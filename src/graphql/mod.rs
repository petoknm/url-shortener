mod schema;
pub mod server;

use std::sync::mpsc::Sender;
use std::sync::Mutex;
use crate::eventsourcing::aggregate::RedirectCommand as Command;

pub struct Context {
    pub cmd_sender: Mutex<Sender<Command>>,
}

impl Context {
    pub fn new(cmd_sender: Sender<Command>) -> Context {
        Context { cmd_sender: Mutex::new(cmd_sender) }
    }

    pub fn send(&self, cmd: Command) {
        let cmd_sender = self.cmd_sender.lock().unwrap();
        cmd_sender.send(cmd);
    }
}

impl juniper::Context for Context {}
