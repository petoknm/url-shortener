use juniper::FieldResult;
use crate::graphql::Context;
use crate::eventsourcing::aggregate::RedirectCommand;

#[derive(GraphQLObject)]
pub struct CreateRedirectResponse {
    error: bool,
}

#[derive(GraphQLObject)]
pub struct Redirect {
    target_url: String,
}

#[derive(GraphQLInputObject)]
pub struct NewRedirect {
    target_url: String,
}

pub struct Query;

graphql_object!(Query: Context |&self| {

    field redirect(&executor, id: String) -> FieldResult<Redirect> {
        Ok(Redirect{target_url: "blah.com".into()})
    }

});

pub struct Mutation;

graphql_object!(Mutation: Context |&self| {

    field createRedirect(&executor, new_redirect: NewRedirect) -> FieldResult<CreateRedirectResponse> {
        let NewRedirect{target_url} = new_redirect;
        executor.context().send(RedirectCommand::Create{access_token: "".into(), source_url: "".into(), target_url});
        Ok(CreateRedirectResponse{error: false})
    }

});

pub type Schema = juniper::RootNode<'static, Query, Mutation>;
