use rocket::response::content;
use rocket::State;

use std::sync::mpsc::Sender;
use crate::eventsourcing::aggregate::RedirectCommand as Command;
use crate::graphql::{Context, schema::{Schema, Query, Mutation}};

#[rocket::get("/")]
fn graphiql() -> content::Html<String> {
    juniper_rocket::graphiql_source("/graphql")
}

#[rocket::get("/graphql?<request>")]
fn get_graphql_handler(
    context: State<Context>,
    request: juniper_rocket::GraphQLRequest,
    schema: State<Schema>,
) -> juniper_rocket::GraphQLResponse {
    request.execute(&schema, &context)
}

#[rocket::post("/graphql", data = "<request>")]
fn post_graphql_handler(
    context: State<Context>,
    request: juniper_rocket::GraphQLRequest,
    schema: State<Schema>,
) -> juniper_rocket::GraphQLResponse {
    request.execute(&schema, &context)
}

pub fn start(cmd_sender: Sender<Command>) {
    rocket::ignite()
        .manage(Context::new(cmd_sender))
        .manage(Schema::new(Query, Mutation))
        .mount("/", rocket::routes![graphiql, get_graphql_handler, post_graphql_handler])
        .launch();
}
