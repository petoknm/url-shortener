pub mod aggregate {
    mod redirect;
    mod id_allocator;

    pub use redirect::*;
    pub use id_allocator::*;
}

pub mod eventstore {
    mod memory;
    pub use memory::Memory;
}

#[derive(Debug, PartialEq)]
pub enum Error {
    CommandRejected,
    EventRejected,
}

pub type Result<T> = std::result::Result<T, Error>;

pub trait Aggregate {
    type Command: Clone;
    type Event: Clone;
    type State;

    fn apply_event(state: Self::State, evt: Self::Event) -> Result<Self::State>;

    fn apply_events(state: Self::State, evts: &[Self::Event]) -> Result<Self::State> {
        evts.iter().cloned().try_fold(state, |state, evt| Self::apply_event(state, evt))
    }

    fn handle_command(state: &Self::State, cmd: Self::Command) -> Result<Vec<Self::Event>>;

    fn execute_commands(state: Self::State, cmds: &[Self::Command]) -> Result<Vec<Self::Event>> {
        let (_state, evts) = cmds.iter().cloned().try_fold((state, vec![]), |(state, mut evts), cmd| {
            let mut new_evts = Self::handle_command(&state, cmd)?;
            let new_state = Self::apply_events(state, &new_evts)?;
            evts.append(&mut new_evts);
            Ok((new_state, evts))
        })?;
        Ok(evts)
    }
}

pub trait EventStore {
    type Event: Clone;

    fn push(&mut self, stream_name: &str, evt: Self::Event);

    fn append(&mut self, stream_name: &str, evts: &[Self::Event]) {
        evts.iter().cloned().for_each(|evt| self.push(stream_name, evt));
    }

    fn get(&self, stream_name: &str) -> Vec<Self::Event>;
}

// struct Dispatcher<E> {
//     event_store: Box<dyn EventStore<Event = E>>
// }
//
// impl<E> Dispatcher<E> {
//
// }
