use std::collections::BTreeMap;
use crate::eventsourcing::EventStore;

pub struct Memory<E>(BTreeMap<String, Vec<E>>);

impl<E> Memory<E> {
    pub fn new() -> Memory<E> {
        Memory(
            BTreeMap::new()
        )
    }
}

impl<E: Clone> EventStore for Memory<E> {
    type Event = E;

    fn push(&mut self, stream_name: &str, evt: E) {
        match self.0.get_mut(stream_name) {
            Some(stream) => { stream.push(evt); },
            None => { self.0.insert(stream_name.into(), vec![evt]); },
        };
    }

    fn get(&self, stream_name: &str) -> Vec<E> {
        match self.0.get(stream_name) {
            Some(stream) => stream.clone(),
            None => vec![],
        }
    }
}
