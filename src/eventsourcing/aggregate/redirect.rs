use crate::eventsourcing::{Aggregate, Result, Error};

#[derive(Clone, Debug, Default, PartialEq)]
pub struct RedirectState {
    access_token: String,
    source_url: String,
    target_url: String,
    deleted: bool,
}

#[derive(Clone, Debug)]
pub enum RedirectCommand {
    Create{access_token: String, source_url: String, target_url: String},
    Update{access_token: String, target_url: String},
    Delete
}

#[derive(Clone, Debug, PartialEq)]
pub enum RedirectEvent {
    Created{access_token: String, source_url: String, target_url: String},
    Updated{target_url: String},
    Deleted,
}

pub struct Redirect;

impl Aggregate for Redirect {
    type Command = RedirectCommand;
    type Event = RedirectEvent;
    type State = RedirectState;

    fn apply_event(mut state: RedirectState, evt: RedirectEvent) -> Result<RedirectState> {
        use RedirectEvent::*;
        match evt {
            Created{access_token, source_url, target_url} => {
                state.access_token = access_token;
                state.source_url   = source_url;
                state.target_url   = target_url;
            },
            Updated{target_url} => state.target_url = target_url,
            Deleted             => state.deleted = true,
        };
        Ok(state)
    }

    fn handle_command(state: &RedirectState, cmd: RedirectCommand) -> Result<Vec<RedirectEvent>> {
        use RedirectCommand::*;
        use RedirectEvent::*;
        match cmd {
            Create{access_token, source_url, target_url} => Ok(vec![Created{access_token, source_url, target_url}]),
            Update{access_token, target_url} => match access_token == state.access_token {
                true  => Ok(vec![Updated{target_url}]),
                false => Err(Error::CommandRejected),
            },
            Delete => Ok(vec![Deleted]),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn cmd_create() {
        assert_eq!(
            Redirect::execute_commands(RedirectState::default(), &[
                RedirectCommand::Create{access_token: "".into(), source_url: "".into(), target_url: "blah.com".into()},
            ]).unwrap(),
            vec![
                RedirectEvent::Created{access_token: "".into(), source_url: "".into(), target_url: "blah.com".into()},
            ]
        );
    }

    #[test]
    fn cmd_update() {
        assert_eq!(
            Redirect::execute_commands(RedirectState::default(), &[
                RedirectCommand::Create{access_token: "".into(), source_url: "".into(), target_url: "blah.com".into()},
                RedirectCommand::Update{access_token: "".into(), target_url: "blah2.com".into()},
            ]).unwrap(),
            vec![
                RedirectEvent::Created{access_token: "".into(), source_url: "".into(), target_url: "blah.com".into()},
                RedirectEvent::Updated{target_url: "blah2.com".into()},
            ]
        )
    }

    #[test]
    fn cmd_delete() {
        assert_eq!(
            Redirect::execute_commands(RedirectState::default(), &[
                RedirectCommand::Create{access_token: "".into(), source_url: "".into(), target_url: "blah.com".into()},
                RedirectCommand::Delete,
            ]).unwrap(),
            vec![
                RedirectEvent::Created{access_token: "".into(), source_url: "".into(), target_url: "blah.com".into()},
                RedirectEvent::Deleted,
            ]
        )
    }
}
