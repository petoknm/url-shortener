use std::collections::BTreeSet;
use crate::eventsourcing::{Aggregate, Result, Error};

#[derive(Debug, Default, PartialEq)]
pub struct IdAllocatorState {
    used_ids: BTreeSet<u64>,
    deleted_ids: BTreeSet<u64>,
}

impl IdAllocatorState {
    pub fn first_deleted_id(&self) -> Option<u64> {
        self.deleted_ids.iter().cloned().nth(0)
    }

    pub fn first_used_id(&self) -> Option<u64> {
        self.used_ids.iter().cloned().nth(0)
    }

    pub fn last_used_id(&self) -> Option<u64> {
        self.used_ids.iter().rev().cloned().nth(0)
    }

    pub fn trim(&mut self) {
        let last_id = self.last_used_id().unwrap_or(0);
        self.deleted_ids.split_off(&last_id);
    }
}

#[derive(Clone, Debug)]
pub enum IdAllocatorCommand {
    AllocateId,
    DeleteId(u64),
}

#[derive(Clone, Debug, PartialEq)]
pub enum IdAllocatorEvent {
    AllocatedId(u64),
    DeletedId(u64),
}

use IdAllocatorCommand::*;
use IdAllocatorEvent::*;

pub struct IdAllocator;

impl Aggregate for IdAllocator {
    type Command = IdAllocatorCommand;
    type Event = IdAllocatorEvent;
    type State = IdAllocatorState;

    fn apply_event(mut state: IdAllocatorState, evt: IdAllocatorEvent) -> Result<IdAllocatorState> {
        match evt {
            AllocatedId(id) => {
                state.used_ids.insert(id);
                state.deleted_ids.remove(&id);
                state.trim();
            },
            DeletedId(id) => {
                state.used_ids.remove(&id);
                state.deleted_ids.insert(id);
                state.trim();
            }
        };
        Ok(state)
    }

    fn handle_command(state: &IdAllocatorState, cmd: IdAllocatorCommand) -> Result<Vec<IdAllocatorEvent>> {
        match cmd {
            AllocateId => match (state.last_used_id(), state.first_deleted_id()) {
                (Some(_), Some(first_deleted_id)) => Ok(vec![AllocatedId(first_deleted_id)]),
                (Some(last_used_id), None)        => Ok(vec![AllocatedId(last_used_id + 1)]),
                (None, _)                         => Ok(vec![AllocatedId(0)]),
            },
            DeleteId(id) => Ok(vec![DeletedId(id)]),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn cmd_allocate_new_id() {
        assert_eq!(
            IdAllocator::execute_commands(IdAllocatorState::default(), &[
                AllocateId
            ]).unwrap(),
            vec![
                AllocatedId(0)
            ]
        );
    }

    #[test]
    fn cmd_delete_id() {
        assert_eq!(
            IdAllocator::execute_commands(IdAllocatorState::default(), &[
                AllocateId,
                DeleteId(0),
            ]).unwrap(),
            vec![
                AllocatedId(0),
                DeletedId(0),
            ]
        );
    }

    #[test]
    fn evt_deleted_id() {
        assert_eq!(
            IdAllocator::apply_events(IdAllocatorState::default(), &[
                AllocatedId(0),
                DeletedId(0),
            ]).unwrap(),
            IdAllocatorState {
                used_ids: [].iter().cloned().collect(),
                deleted_ids: [].iter().cloned().collect()
            }
        )
    }

    #[test]
    fn evt_allocated_deleted_id() {
        assert_eq!(
            IdAllocator::apply_events(IdAllocatorState::default(), &[
                AllocatedId(0),
                DeletedId(0),
                AllocatedId(0),
            ]).unwrap(),
            IdAllocatorState {
                used_ids: [0].iter().cloned().collect(),
                deleted_ids: [].iter().cloned().collect()
            }
        )
    }

    #[test]
    fn can_allocate_deleted_id() {
        assert_eq!(
            IdAllocator::execute_commands(IdAllocatorState::default(), &[
                AllocateId,
                DeleteId(0),
                AllocateId,
            ]).unwrap(),
            vec![
                AllocatedId(0),
                DeletedId(0),
                AllocatedId(0),
            ]
        )
    }
}
